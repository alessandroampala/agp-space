# AGP - Space

## Request
Implement the following features to the project:

1.  **First person view**: render from the point of view of one of the characters in the scene.  
    _How:_  Implement a "toView" function that takes the transform of a camera and applies it (inverted) to all the characters. Pass the transform of one of the characters to this function.
    
2.  **Interface** (testing purpose): in an infinite cycle, read a button, react to it and render a new frame. The interface must be:
Space: switch first person/extern view
WASD + TG: movement of the controlled character (see next point). Rotate (AD), move forward and backward (WS), move up and down (TG).
0..9 buttons: choose the current character to control.

3.  **Simple character control**: move and rotate an arbitrary character in its coordinate system.

4.  **Camera fly effect**: [optional] when pressing 0-9 buttons, or SPACE, make an animation interpolating the camera transform from the initial one to the final one, implementing a "lerp" method of the Transform class.

5.  **Add floor:** add a floor to the scene (a Plane), that gets transformed too (in world/view space) and rendered.
