#pragma once
#include "Vector3.h"
#include "Shapes3D.h"

namespace mgd {

	struct Ray;

	class Camera {
	public:

		Scalar focal;
		int pixelDimX, pixelDimY;

		Camera(Scalar f, int sx, int sy) :focal(f), pixelDimX(sx), pixelDimY(sy) {}

		Ray primaryRay(int pixelX, int pixelY);

	};

}
