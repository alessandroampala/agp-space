#pragma once

#include <iostream>

#include "Vector3.h"
#include "Quaternion.h"

namespace mgd {

	class Transform {
	public:
		Scalar scale;
		Vector3 translate;
		Quaternion rotate;

		Transform(const Vector3& translate = Vector3(), const Quaternion& rotate = Quaternion::identity(), Scalar scale = 1);;

		Vector3 transformPoint(const Vector3& p) const;
		Vector3 transformVersor(const Vector3& p) const;
		Vector3 transformVector(const Vector3& p) const;
		Scalar transformScalar(Scalar p) const;

		Transform inverse() const;

		void invert();

		void move(Vector3 offset);

		void turn(Quaternion offset);

		Vector3 forward() const;

		Vector3 right() const;

		Vector3 up() const;

		static Transform lerp(const Transform& a, const Transform& b, float t);

		friend std::ostream& operator<<(std::ostream& os, Transform const& value)
		{
			os << "Translate: (" << value.translate.x << ", " << value.translate.y << ", " << value.translate.z << ")" << std::endl
				<< "Rotate: (" << value.rotate.im.x << ", " << value.rotate.im.y << ", " << value.rotate.im.z << ", " << value.rotate.re << ")" << std::endl
				<< "Scale: " << value.scale << std::endl;
			return os;
		}
	};


	mgd::Transform operator * (const mgd::Transform& a, const mgd::Transform& b);
	
}
