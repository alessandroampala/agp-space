#include "mgd_utility.h"

// ascii art: convert an intensity value (0 to 1) into a sequence of two chars
const char* mgd::intensityToCstr(Scalar intensity) {
	switch (int(round(intensity * 8))) {
	case 0: return "  "; // darkest
	case 1: return " '";
	case 2: return " +";
	case 3: return " *";
	case 4: return " #";
	case 5: return "'#";
	case 6: return "+#";
	case 7: return "*#";
	case 8: return "##"; // lightest
	default:
	case 10: return "##";
	}
}

const char* mgd::lighting(Versor3 normal) {
	Versor3 lightDir = Versor3(1, 2, -2).normalized();
	// lambertian
	Scalar diffuse = dot(normal, lightDir);
	if (diffuse < 0) diffuse = 0;

	return intensityToCstr(diffuse);
}

void mgd::rayCasting(const std::vector<GameObj*>& objectsVector) {

	Camera c(2.0, 64, 64);

	std::string screenBuffer; // a string to get ready and print all at once

	for (int y = 0; y < c.pixelDimY; y++) {
		for (int x = 0; x < c.pixelDimX; x++) {
			Point3 hitPos;
			Point3 hitNorm;
			Scalar distMax = 1000.0;

			for (const GameObj* g : objectsVector) {
				g->rayCast(c.primaryRay(x, y), hitPos, hitNorm, distMax);
			}

			screenBuffer += lighting(hitNorm);
		}
		screenBuffer += "\n";
	}

	for (GameObj* g : objectsVector)
		delete g; //release tmp memory used for transformed objs

	std::cout << screenBuffer;
}