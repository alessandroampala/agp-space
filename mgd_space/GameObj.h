#pragma once
#include "Transform.h"

namespace mgd {

	struct Ray;

	class GameObj {
	public:
		Transform transform;

		// here add the GameObj content, all expressed in local space

		GameObj(Transform t = Transform()) :
			transform(t)
		{
		}

		virtual void rayCast(const Ray& ray, Point3& hitPos, Versor3& hitNorm, float& distMax) const = 0;
		virtual GameObj* applyTransform(const Transform& t) const = 0;

		virtual ~GameObj() {};
	};

}