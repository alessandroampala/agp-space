#pragma once

#include <vector>

namespace mgd {

	class GameObj;
	class Transform;

	class Scene {
	public:
		std::vector<GameObj*> obj; // a set with GameObj (each with its own transform)

		void populateWithSpheres(int n);
		void populate();
		std::vector<GameObj*> populateWithCharacters();

		// produces a vector of spheres in world space
		std::vector<GameObj*> toWorld() const;
		std::vector<GameObj*> toView(Transform camera) const;
	};
} // end of namespace mgd
