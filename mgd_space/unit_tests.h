#pragma once

#include "Vector3.h"
#include "Transform.h"
#include "Shapes3D.h"

namespace mgd
{
	namespace test
	{
		void unitTestLinearOps();
		void unitTestProdutcs();
		void unitTestRaycasts();
		void unitTestRaycastPlane();
		void examplesOfSyntax();
		void unitTestQuaternions();
		void unitTestTransformation();

		void runUnitTests();
	}
}
