#pragma once

#include "Vector3.h"
#include "Transform.h"
#include "GameObj.h"

namespace mgd {

	struct Ray {
		Point3 p;
		Versor3 d;

		Ray(Point3 _p, Vector3 _d);
		Ray();
	};

	struct Sphere : public GameObj {
		Point3 c;
		Scalar r;

		Sphere(Point3 _c = Vector3::zero(), Scalar _r = 1, Transform t = Transform());
		virtual void rayCast(const Ray& ray, Point3& hitPos, Versor3& hitNorm, float& distMax) const override;
		GameObj* applyTransform(const Transform& t) const override;
	};

	struct Plane : public GameObj {
		Point3 n;
		Vector3 p;

		Plane(Point3 _p, Versor3 _n, Transform t = Transform());
		virtual void rayCast(const Ray& ray, Point3& hitPos, Versor3& hitNorm, float& distMax) const override;
		GameObj* applyTransform(const Transform& t) const override;
	};

	bool rayCast(const Ray& ray, const Sphere& sphere, Point3& hitPos, Versor3& hitNorm, float& distMax);
	bool rayCast(const Ray& ray, const Plane& plane, Point3& hitPos, Versor3& hitNorm, float& distMax);

} // end of namespace
