#include "Shapes3D.h"

bool mgd::rayCast(const Ray& ray, const Sphere& sphere, Point3& hitPos, Versor3& hitNorm, float& distMax) {
	// see exercise on paper...
	// the hitpos is (ray.p + k * ray.dir)
	// for some k such that a*k^2 + b*k + c  = 0
	Scalar a = 1;
	Scalar b = 2 * dot(ray.d, ray.p - sphere.c);
	Scalar c = (ray.p - sphere.c).squaredNorm() - sphere.r * sphere.r;

	Scalar delta = b * b - 4 * a * c;

	if (delta < 0) return false; // ray misses the sphere!

	Scalar k = (-b - sqrt(delta)) / (2 * a);
	if (k < 0) return false;
	if (k > distMax) return false;
	distMax = k;

	hitPos = ray.p + k * ray.d;
	hitNorm = (hitPos - sphere.c).normalized();
	return true;
}

bool mgd::rayCast(const Ray& ray, const Plane& plane, Point3& hitPos, Versor3& hitNorm, float& distMax) {
	Scalar dn = dot(ray.d, plane.n);
	if (dn == 0) return false;

	Scalar k = dot(plane.p - ray.p, plane.n) / dn;

	if (k < 0) return false;
	if (k > distMax) return false;
	distMax = k;
	hitPos = ray.p + k * ray.d;
	hitNorm = plane.n;
	return true;
}

mgd::Ray::Ray()
{

}

mgd::Ray::Ray(Point3 _p, Vector3 _d) : p(_p), d(_d.normalized())
{

}

mgd::Sphere::Sphere(Point3 _c /*= Vector3::zero()*/, Scalar _r /*= 1*/, Transform t /*= Transform()*/) : GameObj(t), c(_c), r(_r)
{

}

void mgd::Sphere::rayCast(const Ray& ray, Point3& hitPos, Versor3& hitNorm, float& distMax) const
{
	mgd::rayCast(ray, *this, hitPos, hitNorm, distMax);
}

mgd::GameObj* mgd::Sphere::applyTransform(const Transform& t) const
{
	return new Sphere(
		t.transformPoint(c),
		t.transformScalar(r)
	);
}

mgd::Plane::Plane(Point3 _p, Versor3 _n, Transform t /*= Transform()*/) : GameObj(t), p(_p), n(_n)
{

}

void mgd::Plane::rayCast(const Ray& ray, Point3& hitPos, Versor3& hitNorm, float& distMax) const
{
	mgd::rayCast(ray, *this, hitPos, hitNorm, distMax);
}

mgd::GameObj* mgd::Plane::applyTransform(const Transform& t) const
{
	return new Plane(t.transformPoint(p), n);
}
