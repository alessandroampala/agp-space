#pragma once

#include <vector>

#include "Vector3.h"
#include "GameObj.h"
#include "Camera.h"

namespace mgd {

	// ascii art: convert an intensity value (0 to 1) into a sequence of two chars
	const char* intensityToCstr(Scalar intensity);

	const char* lighting(Versor3 normal);

	void rayCasting(const std::vector<GameObj*>& objectsVector);

}