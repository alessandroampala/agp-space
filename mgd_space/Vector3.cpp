#include "Vector3.h"

using namespace mgd;

mgd::Vector3 Vector3::random(Scalar range)
{
	return Vector3(
		randomBetween(-range, +range),
		randomBetween(-range, +range),
		randomBetween(-range, +range)
	);
}

void Vector3::normalize()
{
	(*this) /= norm();
}

mgd::Vector3 Vector3::normalized() const
{
	return (*this) / norm();
}

mgd::Scalar Vector3::norm() const
{
	return std::sqrt(squaredNorm());
}

mgd::Scalar Vector3::squaredNorm() const
{
	return x * x + y * y + z * z;
}

bool Vector3::operator==(const Vector3& b)
{
	return (x == x) && (y == y) && (z == z);
}

mgd::Scalar& Vector3::operator[](int i)
{
	static Scalar dummy;
	switch (i) {
	case 0: return x;
	case 1: return y;
	case 2: return z;
	default: assert(0); return dummy;
	}
}

mgd::Scalar Vector3::operator[](int i) const
{
	switch (i) {
	case 0: return x;
	case 1: return y;
	case 2: return z;
	default: assert(0); return 0;
	}
}

mgd::Vector3 Vector3::operator+() const
{
	return Vector3(+x, +y, +z);
}

mgd::Vector3 Vector3::operator-() const
{
	return Vector3(-x, -y, -z);
}

void Vector3::operator-=(const Vector3& v)
{
	x -= v.x; y -= v.y; z -= v.z;
}

mgd::Vector3 Vector3::operator-(const Vector3& v) const
{
	return Vector3(x - v.x, y - v.y, z - v.z);
}

void Vector3::operator+=(const Vector3& v)
{
	x += v.x; y += v.y; z += v.z;
}

mgd::Vector3 Vector3::operator+(const Vector3& v) const
{
	return Vector3(x + v.x, y + v.y, z + v.z);
}

void Vector3::operator/=(Scalar k)
{
	x /= k; y /= k; z /= k;
}

mgd::Vector3 Vector3::operator/(Scalar k) const
{
	return Vector3(x / k, y / k, z / k);
}

void Vector3::operator*=(Scalar k)
{
	x *= k; y *= k; z *= k;
}

mgd::Vector3 Vector3::operator*(Scalar k) const
{
	return Vector3(k * x, k * y, k * z);
}

Vector3::Vector3() :x(0), y(0), z(0)
{

}

Vector3::Vector3(float _x, float _y, float _z) :x(_x), y(_y), z(_z)
{

}

mgd::Vector3 Vector3::lerp(const Vector3& a, const Vector3& b, float t)
{
	return Vector3(mgd::lerp(a.x, b.x, t),
		mgd::lerp(a.y, b.y, t),
		mgd::lerp(a.z, b.z, t));
}

mgd::Vector3 mgd::Vector3::_up = Vector3(0, 1, 0);

const mgd::Vector3& Vector3::right()
{
	return _right;
}

const mgd::Vector3& Vector3::up()
{
	return _up;
}

mgd::Vector3 mgd::Vector3::_right = Vector3(1, 0, 0);

const mgd::Vector3& Vector3::forward()
{
	return _forward;
}

mgd::Vector3 mgd::Vector3::_forward = Vector3(0, 0, 1);

const mgd::Vector3& Vector3::zero()
{
	return _zero;
}

mgd::Vector3 mgd::Vector3::_zero = Vector3(0, 0, 0);