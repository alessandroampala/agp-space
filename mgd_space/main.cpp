#include <iostream>
#include <chrono>
#include <thread>

#include "Vector3.h"
#include "Transform.h"
#include "Shapes3D.h"
#include "Camera.h"
#include "Quaternion.h"
#include "Scene.h"
#include "mgd_utility.h"
#include "unit_tests.h"

using namespace mgd;

void render(const Transform& character, const Scene& s, bool isFirstPerson)
{
	rayCasting(s.toView(character));
	std::cout << "VIEW MODE: " << (isFirstPerson ? "FIRST PERSON" : "THIRD PERSON - MOVEMENT INPUT DISABLED") << std::endl
		<< character;
}

void animation(const Transform& a, const Transform& b, const Scene& s)
{
	int n = 8;
	Transform t;
	for (int i = 0; i < n; ++i)
	{
		t = Transform::lerp(a, b, i * (1.f / n));
		render(t, s, true);
		std::this_thread::sleep_for(std::chrono::milliseconds(500));
	}
}

void handleInput(const Scene& s, Transform** character, Transform& thirdPersonTransform, float movementStep, float turnAngle, bool& firstPerson, const std::vector<GameObj*>& characters)
{
	Vector3 movement;
	Quaternion turn = Quaternion::identity();

	int input = getchar();

	if (input == ' ')
	{
		static Transform* lastCharacter;
		if (firstPerson)
		{
			animation(**character, thirdPersonTransform, s);
			lastCharacter = *character;
			*character = &thirdPersonTransform;
		}
		else
		{
			animation(**character, *lastCharacter, s);
			*character = lastCharacter;
		}

		firstPerson = !firstPerson;
	}

	if (firstPerson && input != '\n')
	{
		if (input >= '0' && input <= '9')
		{
			input -= '0';
			if (input < characters.size())
			{
				if (*character == &characters[input]->transform) return;
				animation(**character, characters[input]->transform, s);
				*character = &characters[input]->transform;
			}
			return;
		}

		switch (input)
		{
			//move input
		case 'w':
			movement = (*character)->forward() * movementStep;
			break;
		case 's':
			movement = -(*character)->forward() * movementStep;
			break;
		case 't':
			movement = (*character)->up() * movementStep;
			break;
		case 'g':
			movement = -(*character)->up() * movementStep;
			break;

			//turn input
		case 'a':
			turn = Quaternion::fromAngleAxis(-turnAngle, Vector3::up());
			break;
		case 'd':
			turn = Quaternion::fromAngleAxis(turnAngle, Vector3::up());
			break;
		}

		(*character)->move(movement);
		(*character)->turn(turn);
	}

	render(**character, s, firstPerson);
}


int main() {

	test::runUnitTests();

	bool isFirstPerson = true;
	const float movementOffset = 1.f;
	const float turnAngle = 22.5f;

	Scene s;
	s.populate();
	std::vector<GameObj*> characters = s.populateWithCharacters();

	Transform thirdPerson(Vector3(0, 0, -6));
	Transform* character = &characters[0]->transform;

	render(*character, s, isFirstPerson);

	while (true)
	{
		handleInput(s, &character, thirdPerson, movementOffset, turnAngle, isFirstPerson, characters);
	}
}
