#include "Camera.h"


mgd::Ray mgd::Camera::primaryRay(int pixelX, int pixelY)
{
	Ray r;
	r.p = Point3(0, 0, 0); // the poinf of view is by definition on the origin
	Scalar clipX = 2.0f * pixelX / pixelDimX - 1.0f;
	Scalar clipY = -2.0f * pixelY / pixelDimY + 1.0f;

	r.d = Vector3(clipX, clipY, focal).normalized();

	return r;
}
