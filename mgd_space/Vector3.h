#pragma once

#include <assert.h>
#include <cmath>

namespace mgd {

	typedef float Scalar;

	inline Scalar randomBetween(Scalar min, Scalar max) {
		return  min + (std::rand() % 1001) / Scalar(1000) * (max - min);
	}

	inline float lerp(float a, float b, float t)
	{
		return a + t * (b - a);
	}

	struct Vector3 {
		Scalar x, y, z;

		Vector3(float _x, float _y, float _z);

		Vector3();

		// linear opreations
		Vector3 operator * (Scalar k) const;
		void operator *= (Scalar k);

		Vector3 operator / (Scalar k) const;
		void operator /= (Scalar k);

		Vector3 operator + (const Vector3& v) const;
		void operator += (const Vector3& v);

		Vector3 operator - (const Vector3& v) const;
		void operator -= (const Vector3& v);

		Vector3 operator - () const;
		Vector3 operator + () const;

		/* accesses to individual coordiantes as a vector */
		Scalar operator[] (int i) const;

		Scalar& operator[] (int i);

		bool operator == (const Vector3& b);

		// norm (aka magnitude, length, intensity, Eucliden norm...)
		Scalar squaredNorm() const;
		Scalar norm() const;
		Vector3 normalized() const;
		void normalize();

		static Vector3 random(Scalar range);
		static const Vector3& up();
		static const Vector3& right();
		static const Vector3& forward();
		static const Vector3& zero();
		
		static Vector3 lerp(const Vector3& a, const Vector3& b, float t);

	private:
		static Vector3 _up;
		static Vector3 _right;
		static Vector3 _forward;
		static Vector3 _zero;
	};


	// vector scaling is commutative!
	inline Vector3 operator * (Scalar k, const Vector3& a) {
		return a * k;
	}


	const Scalar TOLERANCE = 1e-5f;

	inline bool areEqual(Scalar a, Scalar b) {
		return std::abs(a - b) < TOLERANCE;
	}

	inline bool isZero(Scalar a) {
		return std::abs(a) < TOLERANCE;
	}

	inline bool areEqual(const Vector3& a, const Vector3& b) {
		return areEqual(a.x, b.x) && areEqual(a.y, b.y) && areEqual(a.z, b.z);
	}

	inline bool isZero(const Vector3& a) {
		return isZero(a.x) && isZero(a.y) && isZero(a.z);
	}


	inline Scalar dot(const Vector3& a, const Vector3& b) {
		return a.x * b.x + a.y * b.y + a.z * b.z;
	}

	inline Vector3 cross(const Vector3& a, const Vector3& b) {
		return Vector3(
			a.y * b.z - a.z * b.y,
			a.z * b.x - a.x * b.z,
			a.x * b.y - a.y * b.x
		);
	}

	typedef Vector3 Point3;
	typedef Vector3 Versor3;

}; // end of namespace
