#include "Scene.h"
#include "GameObj.h"
#include "Shapes3D.h"


void mgd::Scene::populateWithSpheres(int n)
{
	for (int i = 0; i < n; i++) {
		Sphere* newSphere = new Sphere();
		newSphere->transform.translate = Vector3::random(14) + Vector3(0, 0, 15);
		newSphere->transform.translate.y = 0;
		obj.push_back(newSphere);
	}
}

void mgd::Scene::populate()
{
	Sphere* someoneNew = new Sphere(Vector3::zero(), 1, Transform(Vector3(-1, 0, 0)));
	obj.push_back(someoneNew);
	obj.push_back(new Sphere(Vector3::zero(), 1, Transform(Vector3(1, 0, 2))));
	obj.push_back(new Plane(Vector3::zero(), Vector3::up(), Transform(-Vector3::up())));
}

std::vector<mgd::GameObj*> mgd::Scene::populateWithCharacters()
{
	std::vector<GameObj*> characters;

	const int offset = 8;
	const int n = 8;
	const float angle = 360.f / (float)n;
	for (int i = 0; i < n; ++i)
	{
		Quaternion rotation = Quaternion::fromAngleAxis(angle * i, Vector3::up());
		Vector3 position = rotation.apply(Vector3::forward() * offset);

		Transform t(position);

		float fromRotationAngle = fromToRotation(t.forward(), (Vector3::zero() - t.translate).normalized());

		//shortest path rotation: rotate left or right?
		if (dot(t.right(), (Vector3::zero() - t.translate).normalized()) < 0)
		{
			fromRotationAngle *= -1;
		}

		t.turn(Quaternion::fromAngleAxis(fromRotationAngle, Vector3::up()));



		Sphere* sphere = new Sphere();
		sphere->transform = t;
		obj.push_back(sphere);
		characters.push_back(sphere);
	}
	return characters;
}

std::vector<mgd::GameObj*> mgd::Scene::toWorld() const
{
	std::vector<GameObj*> res;
	res.clear();

	for (GameObj* g : obj) {
		res.push_back(g->applyTransform(g->transform));
		/*res.push_back(apply(g.transform, g.nose));
		res.push_back(apply(g.transform, g.body));*/
	}
	return res;
}

std::vector<mgd::GameObj*> mgd::Scene::toView(Transform camera) const
{
	std::vector<GameObj*> res;
	res.clear();

	for (GameObj* g : obj)
	{
		GameObj* current = g->applyTransform(g->transform);
		res.push_back(current->applyTransform(camera.inverse()));
		delete current;
	}
	return res;
}
