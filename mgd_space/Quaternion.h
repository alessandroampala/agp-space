#pragma once

#define _USE_MATH_DEFINES
#include <math.h>

#include "Vector3.h"

namespace mgd {

	class Quaternion {
	public:
		Vector3 im;
		Scalar re;

		Quaternion(Vector3 _im, Scalar _re);
		Vector3 apply(Vector3 p) const;
		void conjugate();
		Quaternion conjugated() const;
		Quaternion inverse() const;
		mgd::Scalar squaredNorm() const;
		mgd::Scalar norm() const;

		Quaternion operator+(Quaternion rhs) const;
		Quaternion operator -() const;
		Quaternion operator*(mgd::Scalar s) const;
		Quaternion operator/(mgd::Scalar s) const;

		static Quaternion fromVector3(Vector3 v);
		static Quaternion fromAngleAxis(Scalar deg, Vector3 axis);
		static Quaternion identity();

		//the angleBetween between the two quaternions in radians
		static float angleBetween(const Quaternion& a, const Quaternion& b);
		
		static Quaternion lerp(const Quaternion& a, const Quaternion& b, float t);
		static Quaternion slerp(const Quaternion& a, const Quaternion& b, float t);
	};

	Quaternion operator * (const Quaternion& a, const Quaternion& b);

	bool areEqual(const Quaternion& a, const Quaternion& b);
	bool areEquivalent(const Quaternion& a, const Quaternion& b);

	inline float deg2rad(float radians)
	{
		return radians * (180.0f / (float) M_PI);
	}

	inline float rad2deg(float degrees)
	{
		return degrees / (float) M_PI * 180;
	}

	float fromToRotation(Versor3 a, Versor3 b);

}
